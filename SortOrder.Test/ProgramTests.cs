using Xunit;

namespace SortOrder.Test
{
    public class ProgramTests
    {
        [Fact]
        public void Sort_Logic_ExactResult()
        {
            var conditions = new[] { "A<B", "C<D", "B<C" };

            var result = Program.Sort(conditions);

            Assert.Equal("ABCD", result);
        }
    }
}
