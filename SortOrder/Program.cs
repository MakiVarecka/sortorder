﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("SortOrder.Test")]
namespace SortOrder
{
    internal class Program
    {
        internal static void Main(string[] args)
        {
            var conditions = new[] { "A<B", "C<D", "B<C", "F<G", "E<F", "D<E", "G<H" };

            var result = Sort(conditions);

            Console.WriteLine(result);
        }

        internal static string Sort(IEnumerable<string> conditions)
        {
            var chunks = new List<char[]>();

            foreach (var condition in conditions)
            {
                var less = condition[0];
                var greater = condition[2];

                var lessExist = chunks.Any(x => x[^1] == less);
                var greaterExist = chunks.Any(x => x[0] == greater);

                if (lessExist && greaterExist)
                {
                    var lessChunk = chunks.First(x => x[^1] == less);
                    var greaterChunk = chunks.First(x => x[0] == greater);
                    var newChunk = lessChunk.Concat(greaterChunk).ToArray();

                    chunks.Remove(lessChunk);
                    chunks.Remove(greaterChunk);
                    chunks.Add(newChunk);
                }
                else if (lessExist)
                {
                    var lessChunk = chunks.First(x => x[^1] == less);
                    var newChunk = lessChunk.Concat(new[] { greater }).ToArray();

                    chunks.Remove(lessChunk);
                    chunks.Add(newChunk);
                }
                else if (greaterExist)
                {
                    var greaterChunk = chunks.First(x => x[0] == greater);
                    var newChunk = (new[] { less }).Concat(greaterChunk).ToArray();

                    chunks.Remove(greaterChunk);
                    chunks.Add(newChunk);
                }
                else
                {
                    chunks.Add(new[] { less, greater });
                }
            }

            return new string(chunks.First());
        }
    }
}
