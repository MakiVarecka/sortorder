# Description #

My approach can be called something like "tree sort". 

I chose this approach, because this sentence guarantee continuity - this mean, that will be alway one final path. 
> It is guaranteed there is only "<", not ">" in the sequence and that there is always full sequence of characters (there is none missing).

C# is my favourite language, so I chose it. 
The algorithm could be rewritten to for cycles and arrays, but I think, that the main purpose of this task is to show how I think, so I used collection and Linq. Code is much more readable. 